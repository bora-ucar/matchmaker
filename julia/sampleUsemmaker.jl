"
This script creates an 10000 x 10000 sparse matrix (SparseMatrixCSC) 
using SparseArrays, calls mmakerjl function to obtain
a maximum cardinality matching, and reports the ratio of the deficiency of the 
returned matching to 10000.
"

include("mmaker.jl")
using SparseArrays
using StatsBase
using Printf

A = sprand(10000, 10000, 3/10000);

#Single argument, with all default values for the other parameters
match = mmaker.mmakerjl(A);
a = countmap(match);
defi = get(a, -1, 0)
@printf("deficiency %.2f\n", defi/A.m)

#Specify the matching-augmentation algorithm
match = mmaker.mmakerjl(A, matchingAlg_id=5);
a = countmap(match);
defi = get(a, -1, 0)
@printf("deficiency %.2f\n", defi/A.m)

#Specify the initial matching algorithm/heuristic
match = mmaker.mmakerjl(A, initialMatchingAlg_id=5);
a = countmap(match);
defi = get(a, -1, 0)
@printf("deficiency %.2f\n", defi/A.m)


