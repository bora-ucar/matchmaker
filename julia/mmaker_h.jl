function mmakerapi(col_ptrs, col_ids, match, n, m, matching_id, cheap_id, relabel_period, row_match)
# calls the following function from mmaker, in the file matching.c

# void matching(int* col_ptrs, 	int* col_ids, int* match, int* row_match,
#	 		int n, 	int m, 	int matching_id, int cheap_id, 	double relabel_period) 	
    cmatch = fill(Cint(-1), n)

	ret = ccall((:matching, "libmmaker"), Cvoid,
		(Ref{Cint},	Ref{Cint},	Ref{Cint}, Ref{Cint},
		Cint, Cint,	Cint, Cint, Cdouble),
		col_ptrs, col_ids, cmatch, row_match, n, m, matching_id, cheap_id, relabel_period)

	#process the output of matching for use in Julia (1-based indexing)
	for r in 1:m
		if row_match[r] == Cint(-1)
			match[r] = Cint(-1);
		else 
			match[r]  = row_match[r] + Cint(1);
		end
	end

	return
end