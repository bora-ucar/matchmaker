This is a Julia interface to the 
**MatchMaker**: A collection of algorithms for finding maximum transversals or maximum matchings in bipartite graphs.

A shared library needs to be built, using for example ```gcc-9``` on a Mac OS

```
gcc-9 -O3 -c ../cheap.c ../matching.c
gcc-9 -dynamiclib -undefined suppress -flat_namespace cheap.o matching.o -o libmmaker.dylib
```

or on a Linux-like system, one can compile with 

```
gcc -c -fpic ../cheap.c ../matching.c
gcc -shared -o libmmaker.so cheap.o matching.o
```

then, in Julia REPL, 

```
include("sampleUsemmaker.jl")
```

should print something like

```
deficiency 0.08
deficiency 0.08
deficiency 0.08
```


The algorithms are described in the following three papers:

<pre>
@article{klmu:13m,
        Author = {Kaya, Kamer and Langguth, Johannes and Manne, Fredrik and U\c{c}ar, Bora},
        Journal = {Computers \& Operations Research},
        Number = {5},
        Pages = {1266--1275},
        Title = {Push-relabel based algorithms for the maximum transversal problem},
        Volume = {40},
        URL = {https://hal.inria.fr/hal-00763920},
        Year = {2013}
}

@article{duku:12m,
        Author = {Duff, Iain S. and Kaya, Kamer and U\c{c}ar, Bora},
        Journal = {{ACM} Transactions on Mathematical Software},
        Pages = {13:1--13:31},
        Title = {Design, implementation, and analysis of maximum transversal algorithms},
        Volume = {38},
        URL={https://hal.inria.fr/hal-00786548},
        Year = {2012}
}

@inproceedings{pauc:20,
               author = {Ioannis Panagiotas and Bora U{\c{c}}ar},
               booktitle = {28th Annual European Symposium on Algorithms, {ESA} 2020, September 7-9, 2020, Pisa, Italy (Virtual Conference)},
               editor = {Fabrizio Grandoni and Grzegorz Herman and Peter Sanders},
               pages = {76:1--76:23},
               publisher = {Schloss Dagstuhl - Leibniz-Zentrum f{\"{u}}r Informatik},
               series = {LIPIcs},
               title = {Engineering Fast Almost Optimal Algorithms for Bipartite Graph Matching},
               volume = {173},
               year = {2020}
} 
 </pre>
