"
function mmakerjl(A::SparseMatrixCSC; matchingAlg_id::Int=10, initialMatchingAlg_id::Int=3, relabel_period_rate::Float64=1.0, inputMatching::Vector{Integer}=Vector{Integer}(undef, 0))
call with, for example:

```
match = mmaker.mmakerjl(A)
```

A wrapper to the main matching subroutine from MatchMaker.

<pre>
Input: 
	A: A SparseMatrixCSC with (m rows and columns).
	matchingAlg_id: Optional; an integer in 0:10
               0: No augmentation
               1: DFS based
               2: BFS based
               3: MC21 (DFS + lookahead)
               4: PF (Pothen and Fan' algorithm)
               5: PF+ (PF + fairness)
               6: HK (Hopcroft and Karp's algorithm)
               7: HK-DW (Duff-Wiberg implementation of HK)
               8: ABMP (Alt et al.'s algorithm)
               9: ABMP-BFS (ABMP + BFS)
	           10: PR-FIFO-FAIR (DEFAULT)

    initialMatchingAlg_id: Optional; an integer in 0:5
               0: No initialization
               1: Simple Greedy
               2: Karp-Sipser
               3: Random Karp-Sipser (DEFAULT)
               4: Minimum Degree (two-sided)
               5: Truncated random walk
  
	relabel_period_rate: Optional; used only when matchingAlg_id = 10. Otherwise it is ignored.
		       For the PR based algorithm, a global relabeling is started after
        	   every (m+n) x 'matchingAlg_id' pushes where m and
               n are the number of rows and columns of the matrix. Default is 1.0.
                  -1: (minus 1) for a global relabeling after every m pushes;
                  -2: (minus 2) for a global relabeling after every n pushes.
               Other than these two, non-positive values are not allowed.
    inputMatching:  Optional; input matching of size m.  
    		   inputMatching[r]=c: if c = -1, then 
         	   row r is not matched; else column c and row r are matched.
          	   Not verified for correctness. initialMatchingAlg_id has no effect 
          	   (it is set to -1 during the program).
Output: 
	match: a Cint array of size m (match[r] = c for r = 1,...,m)
           if c = -1, then row r is not matched; else column c and row r
           are matched.

</pre>

See also sampleUsemmaker.jl
"
module mmaker

using SparseArrays
include("mmaker_h.jl")

function mmakerjl(A::SparseMatrixCSC; matchingAlg_id::Int=10, initialMatchingAlg_id::Int=3, relabel_period_rate::Float64=1.0, inputMatching::Vector{Integer}=Vector{Integer}(undef, 0))
	matching_id = Cint(matchingAlg_id)
	cheap_id = Cint(initialMatchingAlg_id)
	relabel_period = Cdouble(relabel_period_rate)
	
	checkArguments(matching_id, cheap_id, relabel_period, inputMatching, A.m, A.n)

	n = Cint(A.n)
	m = Cint(A.m)
	colptr = Cint.(A.colptr .- 1) #convert to zero-based indexing
	rowval = Cint.(A.rowval .- 1) #convert to zero-based indexing	

    match = fill(Cint(0), m)
    if length(inputMatching) == 0
		initm = fill(Cint(-1), m)
	else
	    initm = Cint.(inputMatching .- 1)
	    cheap_id = Cint(-1)
    	for r in 1:m
    		if (initm[r] == Cint(-2))
    			initm[r] = Cint(-1)
    		end
    	end
	end
	mmakerapi(colptr, rowval, match, n, m, matching_id, cheap_id, relabel_period, initm)

	return match
end

function checkArguments(matching_id::Cint, cheap_id::Cint, relabel_period::Cdouble, initialMatching::Vector{Integer}, numRows::Integer, numCols::Integer)
	if 	matching_id < 0 || matching_id >10 
		error("matchingAlg_id should be between 0 and 10.")
	end
	if cheap_id < 0 || cheap_id > 5
		error("initialMatching_id should be between 0 and 5.")
	end
	if (matching_id == 10) && ( relabel_period < 0 && relabel_period != Cdouble(-1.0)  && relabel_period != Cdouble(-2.0) )
			error("relabel_period_rate should be -2, -1, or >0")
	end

	if length(initialMatching) == numRows
		for r in 1:numRows
			c = initialMatching[r];
			if !(c == -1 || (c > 0 && c <= numCols))
				error("The given initial matching contains bad numbers")
			end
		end
	elseif length(initialMatching) != 0
		error("The given initial matching is not of the right size")
	end
end

end # module
