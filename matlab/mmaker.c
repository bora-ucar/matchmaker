/**************************************************************************
 * Matlab CMEX-file
 * Developed by K. Kaya, Johannes Langguth and B. Ucar
 *
 * Usage:
 * [match <, exeTime>] = mmaker(A)
 * [match <, exeTime>] = mmaker(A, matchID)
 * [match <, exeTime>] = mmaker(A, matchID, cheapID)
 * [match <, exeTime>] = mmaker(A, matchID, cheapID, relabel_period)
 * [match <, exeTime>] = mmaker(A, matchID, cheapID, relabel_period, init_match)
 )
 * INPUT ARGUMENTS
 * A: a sparse matrix
 * matchID: id of match algo (0-10)
 %      0: No augmentation
 * 		1: DFS based
 *		2: BFS based
 *		3: MC21 (DFS + lookahead)
 *		4: PF (Pothen and Fan' algorithm)
 *		5: PF+ (PF + fairness)
 *		6: HK (Hopcroft and Karp's algorithm)
 *		7: HK-DW (Duff-Wiberg implementation of HK)
 *		8: ABMP (Alt et al.'s algorithm)
 *		9: ABMP-BFS (ABMP + BFS)
 *	   10: PR-FIFO-FAIR (DEFAULT)
 *
 * cheapID: id of cheap algo (0-4)
 *      0: No initialization
 *		1: Simple Greedy
 *		2: Karp-Sipser
 *		3: Random Karp-Sipser (DEFAULT)
 *		4: Minimum Degree (two-sided)
 *      5: Truncated random walk
 *
 * relabel_period: used only when matchID = 10. Otherwise it is ignored.
 *      For the PR based algorithm, a global relabeling is started after
 *      every (m+n) x 'relabel_period' pushes where m and
 * 		n are the number of rows and columns of the matrix. Default is 1.
 * 			"-1": (minus 1) for a global relabeling after every m pushes
 * 			"-2": (minus 2) for a global relabeling after every n pushes
 * 		Other than these two, non-positive values are not allowed.
 *
 * init_match: initial matching of size m. for init_match(r)=c: if c = -1, then 
 *      	   row r is not matched; else column c and row r are matched.
 *             Not verified for correctness. cheapID has not effect (but should 
 *             be in the arguments).
 * 
 * OUTPUT ARGUMENTS
 * match: match of rows match(r) = c for r = 1,...,m;
 *        if c = -1, then row r is not matched; else column c and row r
 *        are matched.
 *
 * exeTime: optional. The total time spent in matching (after converting matlab input to 
 *                    stadard C data types).
 * 
 * Please see the papers:
 *
 * @article{klmu:13m,
 *       Author = {Kaya, Kamer and Langguth, Johannes and Manne, Fredrik and U\c{c}ar, Bora},
 *       Journal = {Computers \& Operations Research},
 *       Number = {5},
 *       Pages = {1266--1275},
 *       Title = {Push-relabel based algorithms for the maximum transversal problem},
 *       Volume = {40},
 *       URL = {https://hal.inria.fr/hal-00763920},
 *       Year = {2013}
 * }
 * @article{duku:12m,
 *       Author = {Duff, Iain S. and Kaya, Kamer and U\c{c}ar, Bora},
 *       Journal = {{ACM} Transactions on Mathematical Software},
 *       Pages = {13:1--13:31},
 *       Title = {Design, implementation, and analysis of maximum transversal algorithms},
 *       Volume = {38},
 *       URL={https://hal.inria.fr/hal-00786548},
 *       Year = {2012}
 * }
 * @inproceedings{pauc:20,
 *              author = {Ioannis Panagiotas and Bora U{\c{c}}ar},
 *              booktitle = {28th Annual European Symposium on Algorithms, {ESA} 2020, September 7-9, 2020, Pisa, Italy (Virtual Conference)},
 *              editor = {Fabrizio Grandoni and Grzegorz Herman and Peter Sanders},
 *              pages = {76:1--76:23},
 *              publisher = {Schloss Dagstuhl - Leibniz-Zentrum f{\"{u}}r Informatik},
 *              series = {LIPIcs},
 *              title = {Engineering Fast Almost Optimal Algorithms for Bipartite Graph Matching},
 *              volume = {173},
 *              year = {2020}
 * }
 * for more details and cite them if you use the codes.
 **************************************************************************/
#include <stdio.h>
#include <math.h>
#include <sys/time.h>

#include "mex.h"
#include "matchmaker.h"

/*inputs*/
#define A_IN            ( prhs[0] )
#define MATCH_IN        ( prhs[1] )
#define CHEAP_IN        ( prhs[2] )
#define RELABEL_IN      ( prhs[3] )
#define INITMATCH_IN    ( prhs[4] )

/*outputs*/
#define MATCH_OUT       ( plhs[0] )
#define TIME_OUT	    ( plhs[1] )

/*...should have the following two lines for matlab version <= 7.1*/
/*
 * typedef int mwIndex;
 * typedef int mwSize;
 */

static double u_wseconds(void) {
    struct timeval tp;
    
    gettimeofday(&tp, NULL);
    
    return (double) tp.tv_sec + (double) tp.tv_usec / 1000000.0;
}


void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
	int i, j, nz;

	mwSize _m, _n, _nz;
	mwIndex *irn_in;
	mwIndex *jcn_in;

	int m, n;
	int *match;
	int *row_match;
	int matchID, cheapID;
	double relabel_period;
	int* col_ptrs;
	int* col_ids;
	double *match_pr;
	double *tmp_pr;
	double etime;

	relabel_period = 1.0;

	if (!(nrhs == 1 || nrhs == 2 || nrhs == 3  || nrhs == 4 || nrhs == 5) || (nlhs != 1 && nlhs != 2)) {
		mexErrMsgTxt("[match <, exeTime>] = mmaker(A, matchID, cheapID, relabel_period, init_match)");
	}

	if (!mxIsSparse(A_IN)) {
		mexErrMsgTxt("First parameter must be a sparse matrix");
	}

	if(nrhs > 1) {
		matchID = (int) ceil(*(mxGetPr(MATCH_IN)));

		if(matchID != *(mxGetPr(MATCH_IN))) {
			mexErrMsgTxt("mmaker: matchID should be an integer\n");
		}

		if(matchID > 10 || matchID < 0) {
			mexErrMsgTxt("mmaker: matchID should be between 0 and 10\n");
		}
	} else {
		matchID = 10;
	}

	if(nrhs > 2) {
		cheapID = (int) ceil(*(mxGetPr(CHEAP_IN)));

		if(cheapID != *(mxGetPr(CHEAP_IN))) {
			mexErrMsgTxt("mmaker: cheapID should be an integer\n");
		}

		if(cheapID > 5 || cheapID < 0) {
			mexErrMsgTxt("mmaker: cheapID should be between 0 and 4\n");
		}
	} else {
		cheapID = 3;
	}

	if(nrhs > 3 && matchID == 10) {
		if ( !(mxGetM(RELABEL_IN) == 1 && mxGetN(RELABEL_IN) == 1) )
			mexErrMsgTxt("mmaker: relabel_period is not a scalar\n");

		relabel_period = *(mxGetPr(RELABEL_IN));
		if(relabel_period <= 0 && relabel_period != -1.0 && relabel_period != -2.0) {
			mexErrMsgTxt("mmaker: relabel_period should be positive or, it should be either -1 or -2\n");
		}		
	}

	_n = mxGetN(A_IN);
	_m = mxGetM(A_IN);
	jcn_in = mxGetJc(A_IN);
	irn_in = mxGetIr(A_IN);
	_nz = jcn_in[_n];

	n = (int)_n; m = (int) _m; nz = (int) _nz;
	if(n != _n || m != _m || nz != _nz) {
		mexErrMsgTxt("mmaker: problems with conversion...will abort.");
	}

	col_ptrs =  (int *) mxCalloc(sizeof(int), (n+1));
	col_ids =(int *) mxCalloc(sizeof(int), (nz));
	for(j = 0; j<=n; j++) {
		col_ptrs[j] = (int) jcn_in[j];
	}
	for(j = 0; j<nz; j++) {
		col_ids[j] = (int) irn_in[j];
	}

	match = (int*) mxCalloc(sizeof(int), n);
	row_match = (int*) mxCalloc(sizeof(int), m);
	if(nrhs > 4)/*read the initial matching in*/
	{
		cheapID = -1; /*no initialization heuristic will be called*/
		if ( !((mxGetM(INITMATCH_IN) == 1 && mxGetN(INITMATCH_IN) == m) || (mxGetM(INITMATCH_IN) == m && mxGetN(INITMATCH_IN) == 1)) )
			mexErrMsgTxt("mmaker: initial matching is not of size 1xm");

		tmp_pr = mxGetPr(INITMATCH_IN);
		for (i = 0; i < m; i++)
			row_match[i] = tmp_pr[i] > 0 ? tmp_pr[i] - 1 : -1;		
	}
	else 
	{
		for (i = 0; i < m; i++) 
			row_match[i] = -1;
	}
	
	etime = -u_wseconds();
	matching(col_ptrs, col_ids, match, row_match, n, m, matchID, cheapID, relabel_period);
	etime += u_wseconds();


	MATCH_OUT = mxCreateDoubleMatrix(m, 1, mxREAL);
	match_pr  = mxGetPr(MATCH_OUT);
	for (i = 0; i < m; i++) {
		if(row_match[i] == -1) {
			match_pr[i] = -1;
		} else {
			match_pr[i] = row_match[i] + 1;
		}
	}
	if (nlhs == 2) 
	{
        TIME_OUT = mxCreateDoubleMatrix(1, 1, mxREAL);
        tmp_pr = mxGetPr(TIME_OUT);
        tmp_pr[0] =  etime;
    }
	mxFree(row_match);
	mxFree(match);
	mxFree(col_ptrs);
	mxFree(col_ids);
	return;
}



