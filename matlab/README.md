# Matlab interface to MatchMaker


The algorithms are described in the following three papers.

<pre>

@article{klmu:13m,
	Author = {Kaya, Kamer and Langguth, Johannes and Manne, Fredrik and U\c{c}ar, Bora},
	Journal = {Computers \& Operations Research},
	Number = {5},
	Pages = {1266--1275},
	Title = {Push-relabel based algorithms for the maximum transversal problem},
	Volume = {40},
	URL = {https://hal.inria.fr/hal-00763920},
	Year = {2013}
}

@article{duku:12m,
	Author = {Duff, Iain S. and Kaya, Kamer and U\c{c}ar, Bora},
	Journal = {{ACM} Transactions on Mathematical Software},
	Pages = {13:1--13:31},
	Title = {Design, implementation, and analysis of maximum transversal algorithms},
	Volume = {38},
	URL={https://hal.inria.fr/hal-00786548},
	Year = {2012}
}

@inproceedings{pauc:20,
              author = {Ioannis Panagiotas and Bora U{\c{c}}ar},
              booktitle = {28th Annual European Symposium on Algorithms, {ESA} 2020, September 7-9, 2020, Pisa, Italy (Virtual Conference)},
              editor = {Fabrizio Grandoni and Grzegorz Herman and Peter Sanders},
              pages = {76:1--76:23},
              publisher = {Schloss Dagstuhl - Leibniz-Zentrum f{\"{u}}r Informatik},
              series = {LIPIcs},
              title = {Engineering Fast Almost Optimal Algorithms for Bipartite Graph Matching},
              volume = {173},
              year = {2020}
}

</pre>

## Build
The codes are written in C and are easily callable. 

In Matlab prompt `mex mmaker.c ../cheap.c ../matching.c` should build the Matlab
callable function.

## Usage and arguments
The function mmaker.m contains relevant help.

```
match = mmaker(A, matchID, cheapID)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Usage:
%   [match <,exeTime>] = mmaker(A)
%   [match <,exeTime>] = mmaker(A, matchID)
%   [match <,exeTime>] = mmaker(A, matchID, cheapID)
%   [match <,exeTime>] = mmaker(A, matchID, cheapID, relabel_period)
%   [match <,exeTime>] = mmaker(A, matchID, cheapID, relabel_period)
%   [match <,exeTime>] = mmaker(A, matchID, cheapID, relabel_period, init_match)

%   INPUT ARGUMENTS
%   A: a sparse matrix
%   matchID: id of match algo (0-10)
% 				0: No augmentation
%               1: DFS based
%               2: BFS based
%               3: MC21 (DFS + lookahead)
%               4: PF (Pothen and Fan' algorithm)
%               5: PF+ (PF + fairness)
%               6: HK (Hopcroft and Karp's algorithm)
%               7: HK-DW (Duff-Wiberg implementation of HK)
%               8: ABMP (Alt et al.'s algorithm)
%               9: ABMP-BFS (ABMP + BFS)
%         	   10: PR-FIFO-FAIR (DEFAULT)
%  
%   cheapID: id of cheap algo (0-5)
%               0: No initialization
%               1: Simple Greedy
%               2: Karp-Sipser
%               3: Random Karp-Sipser (DEFAULT)
%               4: Minimum Degree (two-sided)
%               5: Truncated random walk
%
%   relabel_period: used only when matchID = 10. Otherwise it is ignored.
%        For the PR based algorithm, a global relabeling is started after
%        every (m+n) x 'relabel_period' pushes where m and
%               n are the number of rows and columns of the matrix. Default is 1.
%                       "-1": (minus 1) for a global relabeling after every m pushes
%                       "-2": (minus 2) for a global relabeling after every n pushes
%               Other than these two, non-positive values are not allowed.
%  
%   init_match: initial matching of size m. for init_match(r)=c: if c = -1, then 
%      	   row r is not matched; else column c and row r are matched.
%          Not verified for correctness. cheapID has not effect (but should 
%          be in the arguments).
%
%   OUTPUT ARGUMENTS
%   match: match of rows match(r) = c for r = 1,...,m;
%          if c = -1, then row r is not matched; else column c and row r
%          are matched.
%   exeTime: optional. The total time spent in matching (after converting matlab input to 
%                     stadard C data types).
% 
```

